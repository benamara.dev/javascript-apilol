/*
block A
 1- convertir la classe en variable js
 2- ajouter a la variable une propreité js (.textContent)

block B
 1- la methode js


block C
1- englober la methode dans une fonction

*/


const headerBlague = document.getElementById('header-blague');
const contentBlague = document.getElementById('content-blague');


function lol() {
    fetch(
        "https://api.blablagues.net/?rub=blagues"
    ).then(

        (Response) =>
            (Response.json())
    ).then(
        (infoData) => {
        //  console.log(infoData.data.content.text_hidden)
            headerBlague.textContent = (infoData.data.content.text_head)
            contentBlague.textContent = (infoData.data.content.text !== "")
                ? (infoData.data.content.text)
                : (infoData.data.content.text_hidden);
        }
    )
}
lol();
// document.body.addEventListener('click', function () {
//     console.log("okk");
// })
document.body.addEventListener('click', lol)

